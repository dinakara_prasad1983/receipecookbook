﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeCookBook
{
    /// <summary>
    /// RecipeCookBook
    /// </summary>
    public class RecipeCookBook
    {
        /// <summary>
        /// AllRecipes
        /// </summary>
        public List<AllRecipes> AllRecipes { get; set; }
    }

    /// <summary>
    /// AllRecipes
    /// </summary>
    public class AllRecipes : RecipeData
    {
        public List<IngrediantsData> Ingredaints { get; set; }
    }


}
