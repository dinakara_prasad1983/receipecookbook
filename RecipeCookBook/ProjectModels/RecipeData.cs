﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeCookBook
{
    public class RecipeData
    {
        /// <summary>
        /// RecipeId
        /// </summary>
        [Required]
        public int RecipeId { get; set; }

        /// <summary>
        /// RecipeName
        /// </summary>
        [Required]
        public string RecipeName { get; set; }

        /// <summary>
        /// Desription
        /// </summary>
        [Required]
        public string Desription { get; set;}
    }
}
