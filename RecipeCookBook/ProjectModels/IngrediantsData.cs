﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeCookBook
{
    public class IngrediantsData
    {
        /// <summary>
        /// IngrediantId
        /// </summary>
        public int IngrediantId { get; set; }

        /// <summary>
        /// IngrediantName
        /// </summary>
        [Required]
        public string IngrediantName { get; set; }

        /// <summary>
        /// RecipeId
        /// </summary>
        [Required]
        public int RecipeId { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [Required]
        public string Quantity { get; set; }
             
    }
}
