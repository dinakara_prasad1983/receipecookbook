﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeCookBook
{
    public class ResponseData
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
