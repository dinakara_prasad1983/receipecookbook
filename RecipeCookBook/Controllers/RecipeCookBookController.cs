﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecipeCookBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeCookBook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipeCookBookController : ControllerBase
    {
        public readonly RecipeCookBookContext _recipeCookBookContext;

        public RecipeCookBookController(RecipeCookBookContext recipeCookBookContext)
        {
            this._recipeCookBookContext = recipeCookBookContext;
        }

        /// <summary>
        /// InsertOrUpdateRecipes data
        /// </summary>
        /// <param name="recipe"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Recipe")]
        public async Task<IActionResult> InsertOrUpdateRecipes([FromBody] RecipeData recipe)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (recipe.RecipeId == 0)
                    {
                        long recId = 0;
                        if (_recipeCookBookContext.Recipes.Count() > 0)
                        {
                            recId = _recipeCookBookContext.Recipes.Max(p => p.RecipeId);
                        }
                        Recipe rec = new Recipe();
                        rec.RecipeId = recId + 1;
                        rec.RecipeName = recipe.RecipeName;
                        rec.Description = recipe.Desription;
                        _recipeCookBookContext.Recipes.Add(rec);
                        _recipeCookBookContext.SaveChanges();
                        ResponseData obj = new ResponseData();
                        obj.Success = true;
                        obj.Message = "Recipe added sucessfully";
                        return await Task.Run(() => Ok(obj));
                    }
                    else
                    {
                        var recData = _recipeCookBookContext.Recipes.FirstOrDefault(p => p.RecipeId == recipe.RecipeId);
                        recData.RecipeName = recipe.RecipeName;
                        recData.Description = recipe.Desription;
                        _recipeCookBookContext.SaveChanges();
                        ResponseData obj = new ResponseData();
                        obj.Success = true;
                        obj.Message = "Recipe updated sucessfully";
                        return await Task.Run(() => Ok(obj));
                    }
                }
                catch (Exception)
                {
                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
            else
            {
                return this.BadRequest();
            }
        }

        /// <summary>
        /// InsertOrUpdateIngrediants
        /// </summary>
        /// <param name="ingrediant"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Ingredaint")]
        public async Task<IActionResult> InsertOrUpdateIngrediants([FromBody] IngrediantsData ingrediant)
        {
            if (ModelState.IsValid && ingrediant.RecipeId > 0)
            {
                try
                {
                    if (ingrediant.IngrediantId == 0)
                    {
                        long ingrediantId = 0;
                        if (_recipeCookBookContext.Ingrediants.Count() > 0)
                        {
                            ingrediantId = _recipeCookBookContext.Ingrediants.Max(p => p.IngrediantId);
                        }
                        Ingrediant obj = new Ingrediant();
                        obj.IngrediantId = ingrediantId + 1;
                        obj.RecipeId = ingrediant.RecipeId;
                        obj.IngrediantName = ingrediant.IngrediantName;
                        obj.Quantity = ingrediant.Quantity;
                        _recipeCookBookContext.Ingrediants.Add(obj);
                        _recipeCookBookContext.SaveChanges();
                        ResponseData obj1 = new ResponseData();
                        obj1.Success = true;
                        obj1.Message = "Ingredaint added sucessfully";
                        return await Task.Run(() => Ok(obj1));
                    }
                    else
                    {
                        var ingrdData = _recipeCookBookContext.Ingrediants.FirstOrDefault(p => p.IngrediantId == ingrediant.IngrediantId);
                        ingrdData.IngrediantName = ingrediant.IngrediantName;
                        ingrdData.Quantity = ingrediant.Quantity;
                        _recipeCookBookContext.SaveChanges();
                        ResponseData obj1 = new ResponseData();
                        obj1.Success = true;
                        obj1.Message = "Ingredaint updated sucessfully";
                        return await Task.Run(() => Ok(obj1));
                    }
                }
                catch(Exception)
                {
                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
            else
            {
                return this.BadRequest();
            }
        }

        /// <summary>
        /// RemoveRecipe
        /// </summary>
        /// <param name="recipeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("RemoveRecipe")]
        public async Task<IActionResult> RemoveRecipe([FromQuery] int recipeId)
        {
            if (recipeId > 0)
            {
                try
                {
                    var recipeData = _recipeCookBookContext.Recipes.FirstOrDefault(p => p.RecipeId == recipeId);
                    if (recipeData != null)
                    {
                        var ingredaintsData = _recipeCookBookContext.Ingrediants.Select(p => p).Where(p => p.RecipeId == recipeId);
                        foreach (var item in ingredaintsData)
                        {
                            _recipeCookBookContext.Ingrediants.Remove(item);
                        }
                        _recipeCookBookContext.SaveChanges();

                        _recipeCookBookContext.Recipes.Remove(recipeData);
                        _recipeCookBookContext.SaveChanges();
                    }
                    else
                    {
                        return this.NotFound();
                    }
                    ResponseData obj = new ResponseData();
                    obj.Success = true;
                    obj.Message = "Recipe removed sucessfully";
                    return await Task.Run(() => Ok(obj));
                }
                catch(Exception)
                {
                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
            else
            {
                return this.BadRequest();
            }
        }

        /// <summary>
        /// RemoveIngredaint
        /// </summary>
        /// <param name="ingredaintId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("RemoveIngredaint")]
        public async Task<IActionResult> RemoveIngredaint([FromQuery] int ingredaintId)
        {
            if (ingredaintId > 0)
            {
                try
                {
                    var ingredaintsData = _recipeCookBookContext.Ingrediants.FirstOrDefault(p => p.IngrediantId == ingredaintId);
                    if (ingredaintsData != null)
                    {
                        _recipeCookBookContext.Ingrediants.Remove(ingredaintsData);
                        _recipeCookBookContext.SaveChanges();

                        ResponseData obj = new ResponseData();
                        obj.Success = true;
                        obj.Message = "Ingredaint removed sucessfully";
                        return await Task.Run(() => Ok(obj));
                    }
                    else
                    {
                        return this.NotFound();
                    }
                }
                catch (Exception)
                {
                    return this.StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
            else
            {
                return this.BadRequest();
            }
        }

        /// <summary>
        /// AllRecipes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AllRecipes")]
        public async Task<IActionResult> AllRecipes()
        {
            var recipeData = _recipeCookBookContext.Recipes;
            var ingredaints = _recipeCookBookContext.Ingrediants;

            List<AllRecipes> allRecipes = new List<AllRecipes>();
            foreach (var rec in recipeData)
            {
                AllRecipes obj = new AllRecipes();
                obj.RecipeId = Convert.ToInt32(rec.RecipeId);
                obj.RecipeName = rec.RecipeName;
                obj.Desription = rec.Description;
                List<IngrediantsData> lstIngredaints = new List<IngrediantsData>();
                var ing = ingredaints.Select(p => p).Where(p => p.RecipeId == rec.RecipeId);
                foreach (var t in ing)
                {
                    IngrediantsData data = new IngrediantsData();
                    data.RecipeId = Convert.ToInt32(t.RecipeId);
                    data.IngrediantId = Convert.ToInt32(t.IngrediantId);
                    data.IngrediantName = t.IngrediantName;
                    data.Quantity = t.Quantity;
                    lstIngredaints.Add(data);
                }
                obj.Ingredaints = lstIngredaints;
                allRecipes.Add(obj);
            }
            return await Task.Run(() => Ok(allRecipes));
        }
    }
}
