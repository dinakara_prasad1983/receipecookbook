﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RecipeCookBook.Models
{
    public partial class Recipe
    {
        public Recipe()
        {
            Ingrediants = new HashSet<Ingrediant>();
        }

        public long RecipeId { get; set; }
        public string RecipeName { get; set; }
        public string Description { get; set; }
        public byte[] CreatedDate { get; set; }

        public virtual ICollection<Ingrediant> Ingrediants { get; set; }
    }
}
