﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RecipeCookBook.Models
{
    public partial class Ingrediant
    {
        public long IngrediantId { get; set; }
        public string IngrediantName { get; set; }
        public long? RecipeId { get; set; }
        public string Quantity { get; set; }
        public byte[] CreatedDate { get; set; }

        public virtual Recipe Recipe { get; set; }
    }
}
