﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace RecipeCookBook.Models
{
    public partial class RecipeCookBookContext : DbContext
    {
        public RecipeCookBookContext()
        {
        }

        public RecipeCookBookContext(DbContextOptions<RecipeCookBookContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Ingrediant> Ingrediants { get; set; }
        public virtual DbSet<Recipe> Recipes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlite("DataSource=DBSqlite//RecipeCookBook");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ingrediant>(entity =>
            {
                entity.Property(e => e.IngrediantId)
                    .HasColumnType("INT")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .IsRequired()
                    .HasColumnType("DATETIME")
                    .HasDefaultValueSql("DATE()");

                entity.Property(e => e.IngrediantName)
                    .IsRequired()
                    .HasColumnType("VARCHAR (250)");

                entity.Property(e => e.Quantity)
                    .IsRequired()
                    .HasColumnType("VARCHAR (100)");

                entity.Property(e => e.RecipeId).HasColumnType("INT");

                entity.HasOne(d => d.Recipe)
                    .WithMany(p => p.Ingrediants)
                    .HasForeignKey(d => d.RecipeId);
            });

            modelBuilder.Entity<Recipe>(entity =>
            {
                entity.ToTable("Recipe");

                entity.Property(e => e.RecipeId)
                    .HasColumnType("INT")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("DATETIME")
                    .HasDefaultValueSql("DATE()");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("VARCHAR (10000)");

                entity.Property(e => e.RecipeName)
                    .IsRequired()
                    .HasColumnType("VARCHAR (250)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
